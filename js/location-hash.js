/**
 * @file
 * Main JS file for Location Hash module.
 */

(function ($, Drupal, drupalSettings) {
  $.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    // Work if the element is showing on top 50% of the screen.
    // @todo: this percentage needs to be configurable.
    var extra = 0.5 * (viewportBottom - viewportTop);
    return elementBottom > viewportTop && elementTop < (viewportBottom - extra);
  };

  Drupal.behaviors.LocationHash = {
    // Property set true when animating.
    isAnimating: false,
    attach(context, settings) {

      if (settings.locationHash) {
        // Listen to document scroll if enabled.
        if (settings.locationHash.auto_location_hash) {
          $(document).once('location-hash-scroll').scroll(function () {
            $('a[name]').each(function () {
              // Do not execute if animating.
              if (!Drupal.behaviors.LocationHash.isAnimating) {
                if ($(this).isInViewport()) {
                  var name = $(this).attr('name');
                  var locationHash = '#' + name;
                  // Replace the location hash if found.
                  window.history.replaceState(window.history.state, document.title, locationHash);
                }
              }
            });
          });
        }

        // Perform Smooth Scrolling if enabled.
        if (settings.locationHash.smooth_scroll) {
          // Utility function to perform scrolling.
          function animateScroll(target) {
            Drupal.behaviors.LocationHash.isAnimating = true;
            // @todo: the scroll options needs to be made configuratble.
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1500, 'swing', function () {
              // Function to run when animation is complete.
              Drupal.behaviors.LocationHash.isAnimating = false;
              $(document).trigger('scroll');
            });
          }

          // Smooth scrolling.
          $(document).once('location-hash-smooth-scroll')
            .on('click', 'a[href^="#"]', function (e) {
              e.preventDefault();
              var target = $($.attr(this, 'href'));
              if (target.length) {
                animateScroll(target);
              }
          });
          // Enable smooth scrolling on page load as well.
          $(document).once('smooth-scroll-onload').each(function () {
            var target = $(window.location.hash);
            if (target.length) {
              animateScroll(target);
            }
          });
        }

      }
    }
  };
})(jQuery, Drupal, drupalSettings);
