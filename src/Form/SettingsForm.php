<?php

namespace Drupal\location_hash\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Location Hash settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'location_hash_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['location_hash.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('location_hash.settings');
    $form['auto_location_hash'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable automatic location hash detection on pages.'),
      '#description' => $this->t('When enabled, the url of the page will update automatically according to the section currently in view if page section has a location hash in it.'),
      '#default_value' => $config->get('auto_location_hash'),
    ];
    $form['smooth_scroll'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Smooth Scroll.'),
      '#description' => $this->t('When enabled, any anchor link pointing to a location hash in the page will scroll smoothly.'),
      '#default_value' => $config->get('smooth_scroll'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('location_hash.settings')
      ->set('auto_location_hash', $form_state->getValue('auto_location_hash'))
      ->set('smooth_scroll', $form_state->getValue('smooth_scroll'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
